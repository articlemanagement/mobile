import * as Random from 'expo-random';
import * as Crypto from 'expo-crypto';
import { Buffer } from "buffer";

function base64URLEncode(str: Buffer) {
    return str
        .toString('base64')
        .replace(/\+/g, '-')
        .replace(/\//g, '_')
        .replace(/=/g, '');
}

export const createVerifierChallenge = (): any => {
    return new Promise(async (resolve, reject) => {
        const randomBytes = await Random.getRandomBytesAsync(32);
        const verifier = base64URLEncode(Buffer.from(randomBytes));

        const challengeBase64 = await Crypto.digestStringAsync(
            Crypto.CryptoDigestAlgorithm.SHA256,
            verifier,
            { encoding: Crypto.CryptoEncoding.BASE64 }
        );
        const challenge = challengeBase64
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=/g, '');

        resolve([verifier, challenge]);
    });
};
