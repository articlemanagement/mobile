import Article from "./Article";

export default class Fridge {
    id: number
    name: string
    articles: Array<Article>

    constructor(id: number, name: string, articles: Array<Article>) {
        this.id = id;
        this.name = name;
        this.articles = articles;
    }
}