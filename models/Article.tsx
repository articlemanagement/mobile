export default class Article {
    id: number
    name: string
    barCode: string | undefined
    quantity: number
    photo: string | undefined

    constructor(id: number, barCode: string | undefined, name: string, photo: string | undefined) {
        this.id = id;
        this.name = name;
        this.barCode = barCode;
        this.photo = photo;
        this.quantity = 1;
    }
}