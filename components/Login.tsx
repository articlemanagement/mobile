import React from 'react';
import { Container, Text, Button } from 'native-base';
import { StyleSheet, PointPropType } from 'react-native';
import { createVerifierChallenge } from '../auth/expo-pkce';
import * as AuthSession from 'expo-auth-session';
import * as SecureStore from 'expo-secure-store';
import env from '../.config/env';

interface StringMap {
    [key: string]: string;
}

interface Props {
    navigation: any
}

export default class Login extends React.Component<Props, any> {

    toQueryString(params: StringMap) {
        return Object.entries(params)
            .map(
                ([key, value]) =>
                    `${encodeURIComponent(key)}=${encodeURIComponent(value)}`
            )
            .join("&");
    }

    buildAuthorizeUrl(challenge: string, redirectUrl: string): string {
        return `${env.auth0Domain}/authorize?` + this.toQueryString({
            client_id: env.auth0ClientId,
            response_type: 'code',
            scope: 'openid profile email offline_access',
            redirect_uri: redirectUrl,
            code_challenge: challenge,
            code_challenge_method: 'S256',
            audience: `${env.auth0Domain}/api/v2/`
        });
    }

    buildGetTokenBody(verifier: string, code: string, redirectUrl: string): string {
        const dataToken = new URLSearchParams();
        dataToken.append('client_id', env.auth0ClientId);
        dataToken.append('grant_type', 'authorization_code');
        dataToken.append('redirect_uri', redirectUrl);
        dataToken.append('code_verifier', verifier);
        dataToken.append('code', code);
        return dataToken.toString();
    }

    async getTokenAsync(options: RequestInit) {
        let response = await fetch(`${env.auth0Domain}/oauth/token`, options);
        let data = await response.json()
        return data.access_token;
    }

    /**
     * Authorization Code Flow with Proof Key for Code Exchange (PKCE)
     */
    async login() {
        console.log('loggin in ...');

        /****************************** Authorize call ******************************/
        const redirectUrl = AuthSession.getRedirectUrl();
        const [verifier, challenge] = await createVerifierChallenge();

        const result = await AuthSession.startAsync({
            authUrl: this.buildAuthorizeUrl(challenge, redirectUrl)
        });

        if (result.type !== 'success') {
            return;
        }

        /*************************** Retrieve JWT token ***************************/
        var options = {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
            body: this.buildGetTokenBody(verifier, result.params.code, redirectUrl)
        };

        let response = await fetch(`${env.auth0Domain}/oauth/token`, options);
        let data = await response.json();

        // console.log(data.access_token);

        await SecureStore.setItemAsync('token', data.access_token);

        this.props.navigation.navigate("AppDrawer");
    }

    render() {
        return (
            <Container style={styles.container}>
                <Button light onPress={() => this.login()}>
                    <Text>Login</Text>
                </Button>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})