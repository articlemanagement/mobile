import React from 'react';
import { Content, Container, Button, Text, Icon, View } from 'native-base';
import { Camera, BarCodeScanningResult } from 'expo-camera';
import { StyleSheet, AsyncStorage, Modal } from 'react-native';
import * as SecureStore from 'expo-secure-store';

import { BarCodeScanner } from 'expo-barcode-scanner';

interface Props {
    navigation: any,
    route: any
}

interface State {
    hasCameraPermission: boolean,
    scanned: boolean,
    barCode: BarCodeScanningResult | null,
    fridge: number
}

export default class BarCodeReader extends React.Component<Props, State> {
    camera: Camera | null

    constructor(props: Readonly<Props>) {
        super(props);
        this.camera = null;
        this.state = {
            hasCameraPermission: false,
            scanned: false,
            barCode: null,
            fridge: this.props.route.params.fridge
        }
    }

    getCall(token: string) {
        return {
            method: 'GET',
            headers: {
                "Authorization": "Bearer " + token,
            },
        };
    }

    async componentDidMount() {
        const { status } = await Camera.requestPermissionsAsync();
        this.setState({ hasCameraPermission: status === 'granted' });
    }

    ShowModalFunction(visible: boolean) {
        this.setState({ scanned: visible });
    }

    render() {
        if (this.state.hasCameraPermission === false) {
            return (
                <Container style={styles.container}>
                    <Text>No access to camera</Text>
                </Container>
            );
        }

        return (
            <Container style={{ flex: 1 }}>
                <Camera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={StyleSheet.absoluteFillObject}
                    type={Camera.Constants.Type.back}
                    barCodeScannerSettings={{
                        barCodeTypes: [BarCodeScanner.Constants.BarCodeType.ean13],
                    }}
                    onBarCodeScanned={(e) => this.onBarCodeRead(e)}>
                </Camera>
                <Modal
                    transparent={true}
                    animationType={"slide"}
                    visible={this.state.scanned}
                    onRequestClose={() => { this.ShowModalFunction(!this.state.scanned) }} >

                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                        <View style={styles.modalView}>
                            <Text>Hello</Text>
                        </View>
                    </View>
                </Modal>
            </Container>
        );
    }

    // renderBarCodeData() {
    //     if (this.state.scanned) {
    //         return (
    //             <Text style={styles.barCodeText}> {this.state.barCode?.data} </Text>
    //         );
    //     }
    // }

    // renderConfirmation() {
    //     if (this.state.scanned) {
    //         return (
    //             <Content style={styles.confirmBarCode}>
    //                 <Button rounded onPress={() => this.saveArticle()}>
    //                     <Text>Confirmer</Text>
    //                 </Button>
    //                 <Button rounded icon danger onPress={() => this.resetState()}>
    //                     <Icon name='trash' />
    //                 </Button>
    //             </Content>
    //         );
    //     }
    // }

    // resetState() {
    //     this.setState({ barCode: null, scanned: false });
    //     this.camera?.resumePreview();
    // }

    // async getOpenFoodFactArticle() {
    //     const response = await fetch(`https://fr.openfoodfacts.org/api/v0/product/${this.state.barCode?.data}.json`);
    //     return await response.json();
    // }

    // async saveArticle() {
    //     let openFoodResponse = await this.getOpenFoodFactArticle();
    //     let photo, name;

    //     if (openFoodResponse.status === 1) {
    //         const product = openFoodResponse.product;

    //         photo = product.image_front_url;
    //         name = product.product_name;
    //     } else {
    //         // not found
    //     }

    //     console.log(photo);


    //     const article = new Article(this.state.barCode?.data, photo);
    //     let articlesStr = await AsyncStorage.getItem('articles');
    //     let articles: Array<Article>

    //     if (articlesStr) {
    //         articles = JSON.parse(articlesStr);
    //     } else {
    //         articles = new Array();
    //     }

    //     articles.push(article);

    //     await AsyncStorage.setItem('articles', JSON.stringify(articles));

    //     this.props.navigation.goBack();
    // }

    async onBarCodeRead(result: BarCodeScanningResult) {
        // if (!this.state.scanned) {
        //     this.setState({ barCode: result, scanned: true });
        //     this.camera?.pausePreview();
        // }

        this.props.navigation.navigate('NewArticle', { barCode: result.data, fridge: this.state.fridge });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    barCodeText: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        top: 0,
        textAlign: 'center',
        backgroundColor: 'rgba(255,255,255,0.4)'
    },
    confirmBarCode: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,0.4)'
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
})