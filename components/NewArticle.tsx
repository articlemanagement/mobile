import React from 'react';
import { Container, Form, Item, Input, Content, Icon, Label, Button, Text, Spinner } from 'native-base';
import { StyleSheet, Image, Platform } from 'react-native';
import * as SecureStore from 'expo-secure-store';
import env from '../.config/env';
import DateTimePicker from "@react-native-community/datetimepicker";

interface Props {
    navigation: any,
    route: any
}

interface State {
    isNewArticle: boolean,
    isReady: boolean,
    barCode: string,
    name: string,
    photo: string,
    quantity: string,
    expiryDate: Date,
    fridge: number,
    articleId: number | undefined,
    showDate: boolean
}

export default class NewArticle extends React.Component<Props, State> {

    constructor(props: Readonly<Props>) {
        super(props);
        this.state = {
            isNewArticle: false,
            isReady: false,
            barCode: this.props.route.params.barCode,
            fridge: this.props.route.params.fridge,
            name: '',
            photo: '',
            quantity: '1',
            expiryDate: new Date(),
            articleId: undefined,
            showDate: Platform.OS === 'ios'
        };

        this.setup();
    }

    callApi(method: string, token: string, body?: any) {
        let options: RequestInit = {
            method: method,
            headers: {
                "Authorization": "Bearer " + token,
                'Content-Type': 'application/json'
            },
        };

        if (method === 'POST') {
            options.body = body
        }

        return options;
    }

    async setup() {
        // call API to find article...
        let article = await this.retrieveArticle();

        // if not found, call OpenFoodFact API
        if (!article) {
            this.setState({ isNewArticle: true });
            article = await this.retrieveArticleOFF();
        }

        // If article exist (API or OpenFoodFact)
        if (article) {
            this.setState({ name: article.name, photo: article.photo });
        }

        this.setState({ isReady: true });
    }

    async retrieveArticle() {
        const token: any = await SecureStore.getItemAsync('token');
        let response;

        // check if article already exist in database
        response = await fetch(env.apiUrl + 'articles/' + this.state.barCode, this.callApi('GET', token));

        if (response.status !== 200) {
            return undefined;
        }

        return await response.json();
    }

    async getOpenFoodFactArticle() {
        const response = await fetch(`https://fr.openfoodfacts.org/api/v0/product/${this.state.barCode}.json`);
        return await response.json();
    }

    async retrieveArticleOFF() {
        let openFoodResponse = await this.getOpenFoodFactArticle();

        if (openFoodResponse.status !== 1) {
            return undefined;
        }

        const product = openFoodResponse.product;

        return { name: product.generic_name || '', photo: product.image_front_url || '' };
    }

    async addArticle() {
        console.log('add article...');
        let response;

        const token: any = await SecureStore.getItemAsync('token');

        if (this.state.isNewArticle) {
            // create the article
            const articleToCreate = JSON.stringify({
                article: {
                    bar_code: this.state.barCode,
                    name: this.state.name,
                    photo: this.state.photo
                }
            })

            response = await fetch(env.apiUrl + 'articles', this.callApi('POST', token, articleToCreate));

            if (!response.ok) {
                return;
            }
        }

        // call API to retrieve article id
        let article = await this.retrieveArticle();

        const quantityToAdd = JSON.stringify({ quantity: this.state.quantity })

        // add article to fridge
        response = await fetch(env.apiUrl + 'fridges/' + this.state.fridge + '/' + article.id, this.callApi('POST', token, quantityToAdd));

        if (!response.ok) {
            return;
        }

        this.props.navigation.navigate('Home');
    }

    onDateChange(event: any, selectedDate: Date) {
        const currentDate = selectedDate || this.state.expiryDate;

        this.setState({ expiryDate: currentDate, showDate: Platform.OS === 'ios' });
    }

    render() {
        if (!this.state.isReady) {
            return <Spinner />;
        }

        return (
            <Container>
                <Content>
                    <Form style={{ margin: 15 }}>
                        <Item floatingLabel>
                            <Label>Code barre</Label>
                            <Input disabled value={this.props.route.params.barCode} />
                            <Icon name='barcode' />
                        </Item>
                        <Item floatingLabel>
                            <Label>Nom</Label>
                            <Input
                                value={this.state.name}
                                onChangeText={(newText: string) => this.setState({ name: newText })}
                                disabled={!this.state.isNewArticle} />
                            <Icon name='information-circle' />
                        </Item>
                        <Item floatingLabel last>
                            <Label>Quantité</Label>
                            <Input
                                keyboardType="number-pad"
                                value={this.state.quantity}
                                onChangeText={(newQuantity: string) => this.setState({ quantity: newQuantity.replace(/\D/, '') })} />
                            <Icon name='basket' />
                        </Item>
                        {this.state.photo !== '' && <Image source={{ uri: this.state.photo }} style={{ height: 250, flex: 1, marginTop: 10 }} />}
                        {Platform.OS !== 'ios' &&
                            <><Text>{this.state.expiryDate.toDateString()}</Text>
                                <Button onPress={() => this.setState({ showDate: true })}>
                                    <Text>choix de la date d'expiration</Text>
                                </Button></>}
                        {this.state.showDate && <DateTimePicker
                            locale={"fr"}
                            value={this.state.expiryDate}
                            mode={'date'}
                            display="default"
                            minimumDate={new Date()}
                            onChange={(event, date: any) => this.onDateChange(event, date)}
                        />}
                    </Form>
                    <Button light
                        style={styles.button}
                        onPress={() => this.addArticle()}
                        disabled={this.state.name === '' || this.state.quantity === ''}>
                        <Text>{this.state.isNewArticle ? 'Créer l\'article & l\'ajouter' : 'Ajouter'}</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
        //width: 100,
        marginHorizontal: 30,
        justifyContent: 'center'
    }
})