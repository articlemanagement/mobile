import React from 'react';
import { Container, Text } from 'native-base';
import { StyleSheet } from 'react-native';

export default class About extends React.Component {
    render() {
        return (
            <Container style={styles.container}>
                <Text>About</Text>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})