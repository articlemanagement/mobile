import React from 'react';
import { Container, Text, Button, Badge, View, Spinner, Icon, CardItem, Card, Body, Content, Picker, Form, Item, Label, Input } from 'native-base';
import { createStackNavigator } from '@react-navigation/stack';
import { StyleSheet, Dimensions, Modal, ImageBackground } from 'react-native';
import * as SecureStore from 'expo-secure-store';
import env from '../.config/env';
import Article from '../models/Article';
import Fridge from '../models/Fridge';
import { getFridges, getFridgesPending, getFridgesError } from '../redux/reducers/reducer';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchFridges } from '../redux/actions/fridges';

interface Props {
    navigation: any,
    fetchFridges: Function,
    pending: boolean,
    fridges: Array<Fridge>,
    error: string
}

interface State {
    articlesReady: boolean
    selectedFridge: number | undefined
    articles: Array<Article>
    textVisible: boolean
    barCode: string
}

class Home extends React.Component<Props, State> {
    _unsubscribe: Function | undefined

    constructor(props: Readonly<Props>) {
        super(props);
        this.state = {
            articlesReady: false,
            selectedFridge: undefined,
            articles: new Array(),
            textVisible: false,
            barCode: ''
        }
    }

    callApi(method: string, token: string, body?: any) {
        let options: RequestInit = {
            method: method,
            headers: {
                "Authorization": "Bearer " + token,
                'Content-Type': 'application/json'
            },
        };

        if (method === 'POST' || method === 'DELETE') {
            options.body = body
        }

        return options;
    }

    async componentDidMount() {
        // ONLY FOR RELOAD EXPO
        //this.getFridges();
        //this._unsubscribe = this.props.navigation.addListener('focus', () => this.getFridges());
        this.props.fetchFridges();
    }

    componentWillUnmount() {
        if (this._unsubscribe) { this._unsubscribe() };
    }

    async getFridgesOld() {
        // const token: any = await SecureStore.getItemAsync('token');

        // let response = await fetch(env.apiUrl + 'fridges', this.callApi('GET', token));
        // let data = await response.json();

        // if (data.length > 0)
        //     this.setState({ selectedFridge: data[0].id });

        // this.setState({ fridges: data, isReady: true });
        // this.getFridgeArticles();
    }

    async getFridgeArticles() {
        this.setState({ articlesReady: false });

        const token: any = await SecureStore.getItemAsync('token');

        console.log(token);

        let response = await fetch(env.apiUrl + 'fridges/' + this.state.selectedFridge, this.callApi('GET', token));
        let data = await response.json();

        this.setState({ articles: data.articles, articlesReady: true });
    }

    async addArticle(article: Article) {
        const token: any = await SecureStore.getItemAsync('token');

        const quantity = JSON.stringify({
            quantity: 1
        })

        let response = await fetch(env.apiUrl + 'fridges/' + this.state.selectedFridge + '/' + article.id, this.callApi('POST', token, quantity));

        if (!response.ok) {
            return;
        }

        const articles = this.state.articles;
        const articleFromState = articles.find((a) => a.id == article.id);

        if (!articleFromState)
            return;

        articleFromState.quantity += 1;

        this.setState({ articles: articles });
    }

    async removeArticle(article: Article) {
        const token: any = await SecureStore.getItemAsync('token');

        const quantity = JSON.stringify({
            quantity: 1
        })

        let response = await fetch(env.apiUrl + 'fridges/' + this.state.selectedFridge + '/' + article.id, this.callApi('DELETE', token, quantity));

        if (!response.ok) {
            return;
        }

        const articles = this.state.articles;
        const articleFromState = articles.find((a) => a.id == article.id);

        if (!articleFromState)
            return;

        if (article.quantity > 1) {
            articleFromState.quantity -= 1;
        } else {
            articles.splice(articles.indexOf(articleFromState), 1);
        }

        this.setState({ articles: articles });
    }

    async deleteArticle(article: Article) {
        const token: any = await SecureStore.getItemAsync('token');

        const quantity = JSON.stringify({
            quantity: article.quantity
        })

        let response = await fetch(env.apiUrl + 'fridges/' + this.state.selectedFridge + '/' + article.id, this.callApi('DELETE', token, quantity));

        if (!response.ok) {
            return;
        }

        const articles = this.state.articles;
        const articleFromState = articles.find((a) => a.id == article.id);

        if (!articleFromState)
            return;

        articles.splice(articles.indexOf(articleFromState), 1);

        this.setState({ articles: articles });
    }

    onFridgeChange(value: number) {
        this.setState({
            selectedFridge: value
        });
        this.getFridgeArticles();
    }

    renderFridges() {
        if (this.props.pending) {
            return <Spinner />
        } else {
            return (
                <Form>
                    <Picker
                        mode="dropdown"
                        iosHeader="Select your fridge"
                        iosIcon={<Icon name="arrow-down" />}
                        style={{ width: undefined }}
                        selectedValue={this.state.selectedFridge}
                        onValueChange={this.onFridgeChange.bind(this)}>

                        {this.props.fridges.map(fridge => {
                            return (<Picker.Item label={fridge.name} value={fridge.id} key={fridge.id} />)
                        })}
                    </Picker>
                </Form>
            );
        }
    }

    renderArticlesCard() {
        if (!this.state.articlesReady) {
            return <Spinner />
        } else {
            return (
                <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap', justifyContent: "center" }}>
                    {this.state.articles.map((article, index) => {
                        return (
                            <Card key={index} style={{ width: Dimensions.get('window').width / 2 - 20 }}>
                                <CardItem>
                                    <Body>
                                        <Text>{article.name}</Text>
                                        <Text>{article.barCode}</Text>
                                    </Body>
                                </CardItem>
                                <CardItem cardBody>
                                    {article.photo !== undefined && <ImageBackground source={{ uri: article.photo }} style={{ height: 150, flex: 1 }} resizeMode='stretch'>
                                        <Badge primary style={{ margin: 10 }}>
                                            <Text>{article.quantity}</Text>
                                        </Badge>
                                    </ImageBackground>}
                                </CardItem>
                                <CardItem>
                                    <Body>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Button icon style={{ marginHorizontal: 5, width: 35, height: 35, justifyContent: "center" }}
                                                onPress={() => this.addArticle(article)}>
                                                <Icon active name="add-circle" style={{ marginLeft: 0, marginRight: 0, fontSize: 20 }} />
                                            </Button>
                                            <Button icon warning style={{ marginHorizontal: 5, width: 35, height: 35, justifyContent: "center" }}
                                                onPress={() => this.removeArticle(article)}>
                                                <Icon active name="close-circle" style={{ marginLeft: 0, marginRight: 0, fontSize: 20 }} />
                                            </Button>
                                            <Button icon danger style={{ marginHorizontal: 5, padding: 0, width: 35, height: 35, justifyContent: "center" }}
                                                onPress={() => this.deleteArticle(article)}>
                                                <Icon active name="trash" style={{ marginLeft: 0, marginRight: 0, fontSize: 20 }} />
                                            </Button>
                                        </View>
                                    </Body>
                                </CardItem>
                            </Card>)
                    })}
                </View>
            );
        }
    }

    setTextVisible = (visible: boolean) => {
        this.setState({ textVisible: visible });
    }

    render() {
        const { textVisible } = this.state;

        return (
            <Container>
                <Content>
                    <View style={{ flexDirection: "row" }}>
                        <Button iconLeft style={styles.button} light
                            onPress={() => this.props.navigation.navigate("BarCodeReader", { fridge: this.state.selectedFridge })}>
                            <Icon name='qr-scanner' />
                            <Text>Scanner</Text>
                        </Button>
                        <Button iconLeft style={styles.button} light
                            onPress={() => this.setTextVisible(true)}>
                            <Icon name='text' />
                            <Text>Ecrire</Text>
                        </Button>
                    </View>
                    {this.renderFridges()}
                    {this.renderArticlesCard()}
                    <View style={styles.centeredView}>
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={textVisible}
                            onRequestClose={() => this.setTextVisible(false)}>
                            <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                    <Item floatingLabel>
                                        <Label>Code barre</Label>
                                        <Input
                                            keyboardType="number-pad"
                                            value={this.state.barCode}
                                            onChangeText={(val: string) => this.setState({ barCode: val.replace(/\D/, '') })} />
                                        <Icon name='barcode' />
                                    </Item>

                                    <View style={{ flexDirection: "row", marginTop: 20 }}>
                                        <Button light style={{ margin: 10 }}
                                            disabled={this.state.barCode.length !== 13}
                                            onPress={() => {
                                                this.setTextVisible(!textVisible);
                                                this.props.navigation.navigate('NewArticle', { barCode: this.state.barCode, fridge: this.state.selectedFridge })
                                            }}>
                                            <Text>Confirmer</Text>
                                        </Button>

                                        <Button light style={{ margin: 10 }}
                                            onPress={() => this.setTextVisible(!textVisible)}>
                                            <Text>Annuler</Text>
                                        </Button>
                                    </View>

                                </View>
                            </View>
                        </Modal>
                    </View>
                </Content >
            </Container >
        );
    }
};

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    button: {
        margin: 10
    }
})

const mapStateToProps = (state: any) => ({
    error: getFridgesError(state),
    fridges: getFridges(state),
    pending: getFridgesPending(state)
})

const mapDispatchToProps = (dispatch: any) => bindActionCreators({
    fetchFridges: fetchFridges
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);
