import React from 'react';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'react-native';
import AppStack from './navigation/AppStack';
import { Provider } from 'react-redux';
import store from './redux/store';

interface Props {
    navigation: any
}

interface State {
    isReady: boolean
}

export default class App extends React.Component<Props, State> {

    constructor(props: Readonly<Props>) {
        super(props);
        this.state = {
            isReady: false,
        };

        StatusBar.setBarStyle('dark-content', true);
    }

    async componentDidMount() {
        await Font.loadAsync({
            Roboto: require('native-base/Fonts/Roboto.ttf'),
            Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
            ...Ionicons.font,
        });
        this.setState({ isReady: true });
    }

    render() {
        if (!this.state.isReady) {
            return <AppLoading />;
        }

        return (
            <Provider store={store}>
                <NavigationContainer>
                    <AppStack />
                </NavigationContainer>
            </Provider>
        );
    }
}