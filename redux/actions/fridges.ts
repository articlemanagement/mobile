import * as SecureStore from 'expo-secure-store';

import Fridge from "../../models/Fridge";
import env from "../../.config/env";

export const FETCH_PENDING = 'FETCH_PENDING';
export const FETCH_FRIDGES_SUCCESS = 'FETCH_FRIDGES_SUCCESS';
export const FETCH_ERROR = 'FETCH_FRIDGES_ERROR';

export function fetchFridges() {
    return async function (dispatch: any) {
        dispatch(fetchPending());
        const token: any = await SecureStore.getItemAsync('token');

        try {
            const response = await fetch(env.apiUrl + 'fridges', {
                method: 'GET',
                headers: {
                    "Authorization": "Bearer " + token,
                    'Content-Type': 'application/json'
                },
            });
            const fridges = await response.json();
            dispatch(fetchFridgesSuccess(fridges));
        }
        catch (error) {
            dispatch(fetchError(error));
        }
    }
}

export function addArticle(fridgeId: number, barCode: number, quantity: number, expirationDate: Date) {
    return async function (dispatch: any) {
        dispatch(fetchPending());
        const token: any = await SecureStore.getItemAsync('token');
        const body = JSON.stringify({ quantity, expirationDate });

        try {
            const response = await fetch(env.apiUrl + 'fridges/' + fridgeId + '/' + barCode, {
                method: 'POST',
                headers: {
                    "Authorization": "Bearer " + token,
                    'Content-Type': 'application/json'
                },
                body: body
            });

            if (response.ok) {
                dispatch(fetchAddArticleSuccess(quantity));
            } else {
                dispatch(fetchError('error'));
            }
        }
        catch (error) {
            dispatch(fetchError(error));
        }
    }
}

export function fetchPending() {
    return {
        type: FETCH_PENDING
    }
}

export function fetchError(error: string) {
    return {
        type: FETCH_ERROR,
        error: error
    }
}

export function fetchFridgesSuccess(fridges: Array<Fridge>) {
    return {
        type: FETCH_FRIDGES_SUCCESS,
        payload: fridges
    }
}

export function fetchAddArticleSuccess(quantity: number) {
    return {
        type: FETCH_FRIDGES_SUCCESS,
        payload: { quantity, }
    }
}