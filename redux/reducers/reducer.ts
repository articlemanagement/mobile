import { FETCH_FRIDGES_PENDING, FETCH_FRIDGES_SUCCESS, FETCH_FRIDGES_ERROR } from '../actions/fridges';

interface Action {
    type: string;
    payload: any;
    error: string;
}

interface State {
    pending: boolean;
    fridges: any[];
    error: string;
}

const initialState = {
    pending: false,
    fridges: [],
    error: ''
};

export default function (state: State = initialState, action: Action) {
    switch (action.type) {
        case FETCH_FRIDGES_PENDING:
            return {
                ...state,
                pending: true
            }
        case FETCH_FRIDGES_SUCCESS:
            return {
                ...state,
                pending: false,
                fridges: action.payload,
                error: ''
            }
        case FETCH_FRIDGES_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default:
            return state;
    }
}

// Selectors
export const getFridges = (state: State) => state.fridges;
export const getFridgesPending = (state: State) => state.pending;
export const getFridgesError = (state: State) => state.error;
