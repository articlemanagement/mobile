
const ENV = {
    dev: {
        apiUrl: 'http://192.168.1.25:8080/',
        auth0ClientId: 'l0EWvKPgtDSxTUFc1foUUaS1ZSURki5O',
        auth0Domain: 'https://dev-fo5rwjn9.eu.auth0.com',
    },
    prod: {
        apiUrl: 'http://192.168.1.25:8080/',
        auth0ClientId: 'l0EWvKPgtDSxTUFc1foUUaS1ZSURki5O',
        auth0Domain: 'https://dev-fo5rwjn9.eu.auth0.com'
    }
};

const getEnvVars = (): any => {
    // What is __DEV__ ?
    // This variable is set to true when react-native is running in Dev mode.
    // __DEV__ is true when run locally, but false when published.
    if (__DEV__) {
        return ENV.dev;
    } else {
        return ENV.prod;
    }
};

const env = getEnvVars();

export default env;