import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from '../components/Login';
import AppDrawer from './AppDrawer';

const Stack = createStackNavigator();

export default class AppStack extends React.Component {
    render() {
        return (
            <Stack.Navigator initialRouteName="Login" screenOptions={{
                headerShown: false,
                gestureEnabled: false
            }}>
                <Stack.Screen name="Login" component={Login} options={{ title: 'Connectez vous' }} />
                <Stack.Screen name="AppDrawer" component={AppDrawer} options={{ title: '????' }} />
            </Stack.Navigator>
        );
    }
}