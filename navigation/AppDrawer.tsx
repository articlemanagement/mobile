import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Icon } from 'native-base';
import HomeStack from './HomeStack';
import About from '../components/About';

const Drawer = createDrawerNavigator();

export default class AppDrawer extends React.Component {
    render() {
        return (
            <Drawer.Navigator initialRouteName="Home">
                <Drawer.Screen name="Home" component={HomeStack} options={{
                    title: 'Accueil',
                    drawerIcon: () => {
                        return <Icon name="md-home" style={{ width: 27 }} />
                    }
                }} />
                <Drawer.Screen name="about" component={About} options={{
                    title: 'A propos',
                    drawerIcon: () => {
                        return <Icon name="md-help" style={{ width: 27 }} />
                    }
                }} />
            </Drawer.Navigator>
        );
    }
}