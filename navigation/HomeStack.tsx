import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import BarCodeReader from '../components/BarCodeReader';
import Home from '../components/Home';
import NewArticle from '../components/NewArticle';

const Stack = createStackNavigator();

export default class HomeStack extends React.Component {
    render() {
        return (
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={Home} options={{ title: 'Accueil' }} />
                <Stack.Screen name="BarCodeReader" component={BarCodeReader} options={{ title: 'Scannez le code' }} />
                <Stack.Screen name="NewArticle" component={NewArticle} options={{ title: 'Ajouter un article' }} />
            </Stack.Navigator>
        )
    }
}